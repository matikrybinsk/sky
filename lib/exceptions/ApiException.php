<?php
namespace app\lib\exceptions;

class ApiException extends \Exception
{
    public $errors;
    public $httpCode = 500;
}