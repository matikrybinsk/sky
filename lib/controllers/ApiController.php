<?php
namespace app\lib\controllers;

use yii\web\Controller;

class ApiController extends Controller
{
    public $enableCsrfValidation = false;

}