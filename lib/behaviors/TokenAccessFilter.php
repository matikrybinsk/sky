<?php
namespace app\lib\behaviors;

use app\lib\exceptions\ApiException;
use app\lib\services\AuthorizationService;
use yii\base\ActionFilter;

class TokenAccessFilter extends ActionFilter
{
    public function beforeAction($action)
    {
        $authorization = \Yii::$app->request->headers->get('Authorization');
        if (empty($authorization)) {
            $exception = new ApiException();
            $exception->httpCode = 401;
            throw new $exception;
        }

        list($authType, $token) = explode(' ', $authorization);

        $service = new AuthorizationService();
        if (!$service->authUserByToken($token)) {
            return false;
        }

        return true;
    }
}