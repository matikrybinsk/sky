<?php
namespace app\lib\helpers;

use app\lib\exceptions\ApiException;
use yii\base\Model;
use yii\db\ActiveRecord;

class FormHelper
{
    public static function loadOrFail(Model $model)
    {
        $model->setAttributes(\Yii::$app->request->post());
        self::validateOrFail($model);
        return $model;
    }

    public static function saveOrFail(ActiveRecord $model)
    {
        if (!$model->save()) {
            $apiException = new ApiException('Ошибка сохранения', 500);
            $apiException->errors = $model->getErrors();
            throw $apiException;
        }
    }

    public static function validateOrFail(Model $model)
    {
        if (!$model->validate()) {
            $apiException = new ApiException('Некорректные входные данные', 400);
            $apiException->errors = $model->getErrors();
            throw $apiException;
        }
    }
}

