<?php
namespace app\lib\services;

use app\lib\exceptions\ApiException;
use app\models\Answer;
use app\models\rules\AnswerData;
use app\models\WordTranslationRuEn;
use yii\base\Component;

class AnswerService extends Component
{
    /**
     * @param AnswerData $data
     * @return Answer
     * @throws ApiException
     */
    public function addAnswer(AnswerData $data)
    {
        $isCorrect = $this->isAnswerCorrect($data);
        return $this->saveAnswer($data, $isCorrect);
    }

    public function getAnswersStat($sessionId)
    {
        return $answerStats = Answer::find()
            ->select([
                'status',
                'COUNT(id) as count',
            ])
            ->asArray()
            ->where(['session_id' => $sessionId])
            ->groupBy('status')
            ->indexBy('status')
            ->all();
    }

    private function isAnswerCorrect(AnswerData $data)
    {
        if ($data->translation_type == AnswerData::TRANSLATION_TYPE_RU_EN) {
            $condition = [
                'word_from' => $data->word_id,
                'word_to' => $data->answer_word_id
            ];
        } elseif ($data->translation_type == AnswerData::TRANSLATION_TYPE_EN_RU) {
            $condition = [
                'word_from' => $data->answer_word_id,
                'word_to' => $data->word_id,
            ];
        }

        if (empty($condition)) {
            throw new ApiException('Wrong type');
        }

        return WordTranslationRuEn::find()->where($condition)->exists();
    }

    private function saveAnswer(AnswerData $data, $isCorrect)
    {
        $answer = new Answer();
        $answer->setAttributes($data->getAttributes());
        $answer->setIsCorrect($isCorrect);
        $answer->save();
        return $answer;
    }
}