<?php
namespace app\lib\services;

use app\lib\helpers\FormHelper;
use app\models\rules\LoginData;
use app\models\User;
use Firebase\JWT\JWT;
use yii\base\Component;

class AuthorizationService extends Component
{
    const SECRET_KEY = 'ZUTaY5EV9YFGidKhjA-Z2v1x0agW1m5T';

    private static $currentUser;

    /**
     * @return mixed
     */
    public static function getCurrentUser()
    {
        return self::$currentUser;
    }

    public static function getCurrentUserId()
    {
        return isset(self::$currentUser) ? self::$currentUser['id'] : false;
    }

    /**
     * @param mixed $currentUser
     */
    public static function setCurrentUser($currentUser)
    {
        self::$currentUser = $currentUser;
    }

    public function authorizeUser(LoginData $data)
    {
        $user = User::findOne(['name' => $data->username]);
        if (empty($user)) {
            $user = new User();
            $user->name = $data->username;
            FormHelper::saveOrFail($user);
        }

        $token = [
            'jti' => $user->id,
            'name' => $user->name,
        ];

        return [
            'token' => JWT::encode($token, self::SECRET_KEY),
            'name' => $user->name,
        ];
    }

    public function authUserByToken($token)
    {
        $decoded = (array) JWT::decode($token, self::SECRET_KEY, array('HS256'));
        if (empty($decoded['jti'])) {
            return false;
        }

        $user = User::findOne($decoded['jti']);
        if (!empty($user)) {
            self::setCurrentUser(User::findOne($decoded['jti']));
            return true;
        }
    }
}