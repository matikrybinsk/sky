<?php
namespace app\lib\services;

use app\models\GameSessionUsedTranslation;
use app\models\rules\AnswerData;
use app\models\rules\NextRoundData;
use app\models\structures\TranslateQuestion;
use app\models\WordTranslationRuEn;
use yii\base\Component;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class WordService extends Component
{
    /**
     * @todo Нужно оптимизировать ORDER BY RAND(). Возможно предрасчитывать варианты.
     * @param NextRoundData $data
     * @throws \RuntimeException
     * @return TranslateQuestion
     */
    public function getNextRound(NextRoundData $data)
    {
        $translationType = $this->getTranslationType();

        if ($translationType == AnswerData::TRANSLATION_TYPE_RU_EN) {
            $correntAttribute = 'wordFrom';
            $variantsAttribute = 'wordTo';
        } else {
            $correntAttribute = 'wordTo';
            $variantsAttribute = 'wordFrom';
        }

        $usedTranslationQuery = GameSessionUsedTranslation::find()
            ->select(['translation_id'])
            ->where(['session_id' => $data->session_id]);

        $correntPair = WordTranslationRuEn::find()
            ->where(['NOT IN', 'id', $usedTranslationQuery])
            ->orderBy(new Expression('RAND()'))
            ->limit(1)
            ->with(['wordFrom', 'wordTo'])
            ->asArray()
            ->one();


        if (empty($correntPair)) {
            throw new \RuntimeException('Закончились вопросы');
        }

        $additionalVariants = WordTranslationRuEn::find()
            ->alias('t')
            ->where(['NOT IN', 't.id', [$correntPair['id']]])
            ->orderBy(new Expression('RAND()'))
            ->limit(3)
            ->innerJoinWith($variantsAttribute)
            ->asArray()
            ->all();


        $question = new TranslateQuestion();
        $question->word = $correntPair[$correntAttribute];
        $question->variants = ArrayHelper::getColumn($additionalVariants, $variantsAttribute);
        $question->variants[] = $correntPair[$variantsAttribute];
        $question->translation_type = $translationType;
        shuffle($question->variants);

        $this->markTranslationAsUsed($data->session_id, $correntPair['id']);

        return $question;
    }

    private function getTranslationType()
    {
        return rand(0, 1) == 0 ? AnswerData::TRANSLATION_TYPE_RU_EN : AnswerData::TRANSLATION_TYPE_EN_RU;
    }

    private function markTranslationAsUsed($sessionId, $pairId)
    {
        $usedTranslation = new GameSessionUsedTranslation();
        $usedTranslation->session_id = $sessionId;
        $usedTranslation->translation_id = $pairId;
        $usedTranslation->save();
    }
}