<?php

use yii\db\Migration;

class m160828_144030_init extends Migration
{
    public function up()
    {
        $this->db->createCommand("
            CREATE TABLE answer (
              id int(11) NOT NULL AUTO_INCREMENT,
              session_id int(11) NOT NULL,
              word_id int(11) NOT NULL,
              answer_word_id int(11) NOT NULL,
              status enum ('CORRECT', 'WRONG') NOT NULL,
              translation_type varchar(10) DEFAULT NULL,
              PRIMARY KEY (id),
              INDEX IDX_answer_game_session_id (session_id)
            )
            ENGINE = INNODB
            AUTO_INCREMENT = 140
            AVG_ROW_LENGTH = 2340
            CHARACTER SET utf8
            COLLATE utf8_general_ci;

            CREATE TABLE game_session (
              id int(11) NOT NULL AUTO_INCREMENT,
              user_id int(11) NOT NULL,
              answers_correct int(11) DEFAULT 0,
              answers_wrong int(11) DEFAULT 0,
              allowed_errors int(11) NOT NULL DEFAULT 3,
              status enum ('OPEN', 'CLOSED') DEFAULT 'OPEN',
              PRIMARY KEY (id),
              INDEX IDX_game_session_user_id (user_id)
            )
            ENGINE = INNODB
            AUTO_INCREMENT = 76
            AVG_ROW_LENGTH = 1365
            CHARACTER SET utf8
            COLLATE utf8_general_ci;

            CREATE TABLE user (
              id int(11) NOT NULL AUTO_INCREMENT,
              name varchar(50) NOT NULL,
              PRIMARY KEY (id),
              UNIQUE INDEX UK_user_name (name)
            )
            ENGINE = INNODB
            AUTO_INCREMENT = 65
            CHARACTER SET utf8
            COLLATE utf8_general_ci;

            CREATE TABLE word (
              id int(11) NOT NULL AUTO_INCREMENT,
              word varchar(255) NOT NULL,
              PRIMARY KEY (id)
            )
            ENGINE = INNODB
            AUTO_INCREMENT = 38
            AVG_ROW_LENGTH = 481
            CHARACTER SET utf8
            COLLATE utf8_general_ci;

            CREATE TABLE word_translation_ru_en (
              id int(11) NOT NULL AUTO_INCREMENT,
              word_from int(11) DEFAULT NULL,
              word_to int(11) DEFAULT NULL,
              PRIMARY KEY (id),
              INDEX IDX_word_translation_ru_en_word_from (word_from),
              INDEX IDX_word_translation_ru_en_word_to (word_to),
              CONSTRAINT FK_word_translation_ru_en_word_id FOREIGN KEY (word_from)
              REFERENCES word (id) ON DELETE CASCADE ON UPDATE CASCADE
            )
            ENGINE = INNODB
            AUTO_INCREMENT = 19
            AVG_ROW_LENGTH = 963
            CHARACTER SET utf8
            COLLATE utf8_general_ci;

            CREATE TABLE game_session_used_translation (
              session_id int(11) NOT NULL,
              translation_id int(11) NOT NULL,
              PRIMARY KEY (session_id, translation_id),
              CONSTRAINT FK_game_session_used_translation_game_session_id FOREIGN KEY (session_id)
              REFERENCES game_session (id) ON DELETE CASCADE ON UPDATE CASCADE,
              CONSTRAINT FK_game_session_used_translation_word_translation_ru_en_id FOREIGN KEY (translation_id)
              REFERENCES word_translation_ru_en (id) ON DELETE RESTRICT ON UPDATE RESTRICT
            )
            ENGINE = INNODB
            CHARACTER SET utf8
            COLLATE utf8_general_ci;


INSERT INTO word(id, word) VALUES
(4, 'яблоко');
INSERT INTO word(id, word) VALUES
(5, 'apple');
INSERT INTO word(id, word) VALUES
(6, 'слива');
INSERT INTO word(id, word) VALUES
(7, 'pear');
INSERT INTO word(id, word) VALUES
(8, 'апельсин');
INSERT INTO word(id, word) VALUES
(9, 'orange');
INSERT INTO word(id, word) VALUES
(10, 'виноград');
INSERT INTO word(id, word) VALUES
(11, 'grape');
INSERT INTO word(id, word) VALUES
(12, 'лимон');
INSERT INTO word(id, word) VALUES
(13, 'lemon');
INSERT INTO word(id, word) VALUES
(14, 'ананас');
INSERT INTO word(id, word) VALUES
(15, 'pineapple');
INSERT INTO word(id, word) VALUES
(16, 'арбуз');
INSERT INTO word(id, word) VALUES
(17, 'watermelon');
INSERT INTO word(id, word) VALUES
(18, 'кокос');
INSERT INTO word(id, word) VALUES
(19, 'coconut');
INSERT INTO word(id, word) VALUES
(20, 'банан');
INSERT INTO word(id, word) VALUES
(21, 'banana');
INSERT INTO word(id, word) VALUES
(22, 'помело');
INSERT INTO word(id, word) VALUES
(23, 'pomelo');
INSERT INTO word(id, word) VALUES
(24, 'клубника');
INSERT INTO word(id, word) VALUES
(25, 'strawberry');
INSERT INTO word(id, word) VALUES
(26, 'малина');
INSERT INTO word(id, word) VALUES
(27, 'raspberry');
INSERT INTO word(id, word) VALUES
(28, 'дыня');
INSERT INTO word(id, word) VALUES
(29, 'melon');
INSERT INTO word(id, word) VALUES
(30, 'абрикос');
INSERT INTO word(id, word) VALUES
(31, 'apricot');
INSERT INTO word(id, word) VALUES
(32, 'манго');
INSERT INTO word(id, word) VALUES
(33, 'mango');
INSERT INTO word(id, word) VALUES
(34, 'гранат');
INSERT INTO word(id, word) VALUES
(35, 'pomegranate');
INSERT INTO word(id, word) VALUES
(36, 'вишня');
INSERT INTO word(id, word) VALUES
(37, 'cherry');


INSERT INTO word_translation_ru_en(id, word_from, word_to) VALUES
(2, 4, 5);
INSERT INTO word_translation_ru_en(id, word_from, word_to) VALUES
(3, 6, 7);
INSERT INTO word_translation_ru_en(id, word_from, word_to) VALUES
(4, 8, 9);
INSERT INTO word_translation_ru_en(id, word_from, word_to) VALUES
(5, 10, 11);
INSERT INTO word_translation_ru_en(id, word_from, word_to) VALUES
(6, 12, 13);
INSERT INTO word_translation_ru_en(id, word_from, word_to) VALUES
(7, 14, 15);
INSERT INTO word_translation_ru_en(id, word_from, word_to) VALUES
(8, 16, 17);
INSERT INTO word_translation_ru_en(id, word_from, word_to) VALUES
(9, 18, 19);
INSERT INTO word_translation_ru_en(id, word_from, word_to) VALUES
(10, 20, 21);
INSERT INTO word_translation_ru_en(id, word_from, word_to) VALUES
(11, 22, 23);
INSERT INTO word_translation_ru_en(id, word_from, word_to) VALUES
(12, 24, 25);
INSERT INTO word_translation_ru_en(id, word_from, word_to) VALUES
(13, 26, 27);
INSERT INTO word_translation_ru_en(id, word_from, word_to) VALUES
(14, 28, 29);
INSERT INTO word_translation_ru_en(id, word_from, word_to) VALUES
(15, 30, 31);
INSERT INTO word_translation_ru_en(id, word_from, word_to) VALUES
(16, 32, 33);
INSERT INTO word_translation_ru_en(id, word_from, word_to) VALUES
(17, 34, 35);
INSERT INTO word_translation_ru_en(id, word_from, word_to) VALUES
(18, 36, 37);


            ")->execute();
    }

    public function down()
    {
        return false;
    }
}
