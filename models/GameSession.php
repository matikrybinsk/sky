<?php
namespace app\models;

use app\lib\services\AuthorizationService;
use yii\db\ActiveRecord;

/**
 * Class GameSession
 * @package app\models
 * @property int $user_id
 * @property int $answers_correct
 * @property int $answers_wrong
 * @property int $status
 * @property int $allowed_errors
 */
class GameSession extends ActiveRecord
{
    const STATUS_OPEN = 'OPEN';
    const STATUS_CLOSE = 'CLOSED';

    public static function tableName()
    {
        return 'game_session';
    }

    public function rules()
    {
        return [
            [['id', 'user_id', 'answers_correct', 'answers_wrong'], 'integer'],
            [['user_id'], 'required'],
            [['status'], 'string']
        ];
    }

    public function isClosed()
    {
        return $this->status == self::STATUS_CLOSE;
    }

    public function isEnoughErrors()
    {
        return $this->answers_wrong >= $this->allowed_errors;
    }

    /**
     * Create with some default settings
     * @param array $settings
     * @return GameSession
     */
    public static function create($settings = [])
    {
        $session = new GameSession();
        $session->user_id = AuthorizationService::getCurrentUserId();
        $session->allowed_errors = 3;
        $session->status = self::STATUS_OPEN;
        $session->setAttributes($settings);
        return $session;
    }

    public function getNumericAttribute($attributeName)
    {
        if (empty($this->{$attributeName})) {
            return 0;
        }
        return (int) $this->{$attributeName};
    }
}