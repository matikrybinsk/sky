<?php
namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class GameSession
 * @package app\models
 * @property int $session_id
 * @property int $translation_id
 */
class GameSessionUsedTranslation extends ActiveRecord
{
    public static function tableName()
    {
        return 'game_session_used_translation';
    }

    public function rules()
    {
        return [
            [['session_id', 'translation_id'], 'integer'],
            [['session_id', 'translation_id'], 'required'],
        ];
    }
}