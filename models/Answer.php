<?php
namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Answer
 * @package app\models
 * @property string $status
 */
class Answer extends ActiveRecord
{
    const STATUS_CORRECT = 'CORRECT';
    const STATUS_WRONG = 'WRONG';

    public static function tableName()
    {
        return 'answer';
    }

    public function rules()
    {
        return [
            [['id', 'session_id', 'word_id', 'answer_word_id'], 'integer'],
            [['translation_type'], 'string'],
            [['status'], 'required'],
        ];
    }

    public function isCorrect()
    {
        return $this->status == self::STATUS_CORRECT;
    }

    public function setIsCorrect($isCorrect)
    {
        $this->status = $isCorrect ? self::STATUS_CORRECT : self::STATUS_WRONG;
    }
}