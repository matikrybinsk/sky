<?php
namespace app\models;

use yii\db\ActiveRecord;

class WordTranslationRuEn extends ActiveRecord
{
    public static function tableName()
    {
        return 'word_translation_ru_en';
    }

    public function rules()
    {
        return [
            [['id', 'word_from', 'word_to'], 'integer'],
            [['word_from', 'word_to'], 'required'],
        ];
    }

    public function getWordFrom()
    {
        return $this->hasOne(Word::className(), ['id' => 'word_from']);
    }

    public function getWordTo()
    {
        return $this->hasOne(Word::className(), ['id' => 'word_to']);
    }
}