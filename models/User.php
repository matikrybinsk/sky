<?php
namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class User
 * @package app\models
 * @property $name string
 * @property $id int
 */
class User extends ActiveRecord
{
    public static function tableName()
    {
        return 'user';
    }

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name'], 'string'],
            [['name'], 'required'],
            [['name'], 'unique'],
        ];
    }
}