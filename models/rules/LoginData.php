<?php

namespace app\models\rules;

use Yii;
use yii\base\Model;

class LoginData extends Model
{
    public $username;

    public function rules()
    {
        return [
            [['username'], 'required', 'message' => 'Имя не может быть пустым'],
        ];
    }
}
