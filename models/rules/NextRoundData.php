<?php

namespace app\models\rules;

use Yii;
use yii\base\Model;

/**
 * Class NextRoundData
 * @property int $translation_id
 */
class NextRoundData extends Model
{
    public $session_id;

    public function rules()
    {
        return [
            [['session_id'], 'required', 'message' => 'Не указан идентификатор сесссии'],
        ];
    }
}
