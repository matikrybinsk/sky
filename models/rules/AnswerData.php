<?php

namespace app\models\rules;

use Yii;
use yii\base\Model;


class AnswerData extends Model
{
    const TRANSLATION_TYPE_RU_EN = 'ru-en';
    const TRANSLATION_TYPE_EN_RU = 'en-ru';

    public $session_id;
    public $word_id;
    public $answer_word_id;
    public $translation_type;

    public function rules()
    {
        return [
            [['session_id', 'word_id', 'answer_word_id', 'translation_type'], 'required', 'message' => 'Не указаны обязательные поля'],
        ];
    }
}
