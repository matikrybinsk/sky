<?php

namespace app\models\structures;

use yii\base\Model;

class TranslateQuestion extends Model
{
    public $word = [];
    public $variants = [];
    public $translation_type;
    public $errors = [];
}
