<?php
namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Word
 * @package app\models
 * @property string $word
 */
class Word extends ActiveRecord
{
    public static function tableName()
    {
        return 'word';
    }

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['word'], 'string'],
            [['word'], 'required'],
        ];
    }
}