<?php

namespace app\controllers;

use app\lib\controllers\ApiController;
use app\lib\helpers\FormHelper;
use app\lib\services\AuthorizationService;
use app\models\rules\LoginData;
use Yii;

class AuthController extends ApiController
{
    public function actionLogin()
    {
        $data = FormHelper::loadOrFail(new LoginData());

        $authService = new AuthorizationService();
        return $authService->authorizeUser($data);
    }
}
