<?php

namespace app\controllers;

use app\lib\behaviors\TokenAccessFilter;
use app\lib\controllers\ApiController;
use app\lib\helpers\FormHelper;
use app\lib\services\AnswerService;
use app\lib\services\AuthorizationService;
use app\lib\services\WordService;
use app\models\Answer;
use app\models\GameSession;
use app\models\rules\AnswerData;
use app\models\rules\NextRoundData;
use Yii;

class GameSessionController extends ApiController
{
    public function behaviors()
    {
        return [
            'tokenAccess' => [
                'class' => TokenAccessFilter::className()
            ],
        ];
    }

    public function actionGetSession()
    {
        $restoreSessionId = Yii::$app->request->post('game_session_id');
        if ($restoreSessionId) {
            /**
             * @var $gameSession GameSession
             */
            $gameSession = GameSession::find()->where([
                'id' => $restoreSessionId,
                'user_id' => AuthorizationService::getCurrentUserId()
            ])->one();
        }

        if (empty($gameSession)) {
            $gameSession = GameSession::create();
            FormHelper::saveOrFail($gameSession);
        }

        return [
            'session_id' => $gameSession->id,
            'answers_correct' => $gameSession->getNumericAttribute('answers_correct'),
            'answers_wrong' => $gameSession->getNumericAttribute('answers_wrong'),
            'allowed_errors' => $gameSession->getNumericAttribute('allowed_errors'),
            'points' => $gameSession->getNumericAttribute('answers_correct'),
            'status' => $gameSession->status,
        ];
    }

    public function actionNextRound()
    {
        $data = FormHelper::loadOrFail(new NextRoundData());
        try {
            $wordService = new WordService();
            $nextQuestion = $wordService->getNextRound($data);
            return (array) $nextQuestion;
        } catch (\RuntimeException $e) {
            return [
                'success' => false,
                'errors' => $e->getMessage(),
            ];
        }
    }

    public function actionAnswer()
    {
        /**
         * @var $data AnswerData
         * @var $gameSession GameSession
         */
        $data = FormHelper::loadOrFail(new AnswerData());
        $gameSession = GameSession::findOne(['id' => $data->session_id]);
        if ($gameSession->isClosed()) {
            return [
                'status' => $gameSession->status,
            ];
        }

        $service = new AnswerService();
        $answer = $service->addAnswer($data);

        // Обновляем статистику ответов в  ессии
        $answerStats = $service->getAnswersStat($gameSession['id']);

        $gameSession['answers_wrong'] = isset($answerStats[Answer::STATUS_WRONG]) ? $answerStats[Answer::STATUS_WRONG]['count'] : 0;
        $gameSession['answers_correct'] = isset($answerStats[Answer::STATUS_CORRECT]) ? $answerStats[Answer::STATUS_CORRECT]['count'] : 0;
        if ($gameSession->isEnoughErrors()) {
            $gameSession->status = GameSession::STATUS_CLOSE;
        }
        $gameSession->save();

        $result = [
            'session' => [
                'answers_correct' => $gameSession->getNumericAttribute('answers_correct'),
                'answers_wrong' => $gameSession->getNumericAttribute('answers_wrong'),
                'allowed_errors' => $gameSession->getNumericAttribute('allowed_errors'),
                'points' => $gameSession->getNumericAttribute('answers_correct'),
                'status' => $gameSession->status,
            ],
            'correct' => $answer->isCorrect(),
        ];

        return $result;
    }


}
