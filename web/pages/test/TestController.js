app.controller('TestController', function ($rootScope, $scope, $http, API, session, GameSession, $timeout, $state, AuthService) {
    $scope.session = session;
    $scope.blockUserInteraction = false;

    nextQuestion();

    $scope.Answer = function (wordId) {
        if ($scope.blockUserInteraction) {
            return;
        }

        $scope.blockUserInteraction = true;
        GameSession.answer(wordId).then(function (result) {
            GameSession.updateSessionStats(result.session);

            if (result.correct) {
                handleCorrectAnswer(wordId, result);
            } else {
                handleWrongAnswer(wordId, result);
            }
        }, function () {
            $scope.blockUserInteraction = false;
        });
    };

    $scope.Logout = function () {
        AuthService.logout();
        GameSession.newSession();
        $state.go('login');
    };

    function handleCorrectAnswer(wordId, result) {
        $scope.correctId = wordId;
        $scope.wrongId = null;

        $timeout(1000).then(function () {
            $scope.blockUserInteraction = false;
            nextQuestion();
        });
    }

    function handleWrongAnswer(wordId, result) {
        $scope.wrongId = wordId;
        $scope.correctId = null;
        $timeout(1000).then(function () {
            $scope.blockUserInteraction = false;

            // Возможно ошибок слишком много
            if (result.session.status == 'CLOSED') {
                $state.go('gameover');
            }
        });
    }

    function nextQuestion() {
        $scope.correctId = null;
        $scope.wrongId = null;

        GameSession.nextQuestion().then(function (result) {
            // Закончились вопросы
            if (result.success == false) {
                return $state.go('gameover');
            }
            $scope.currentQuestion = result;
        });
    }

});