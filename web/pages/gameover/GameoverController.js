app.controller('GameoverController', function ($scope, session, GameSession, $state) {
    $scope.session = session;

    $scope.PlayAgain = function () {
        GameSession.newSession();
        $state.go('test');
    };
});