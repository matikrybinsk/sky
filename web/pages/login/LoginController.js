app.controller('LoginController', function ($rootScope, $scope, $http, AuthService, $state) {
    $scope.username = 'Гость - ' + (new Date().getTime());
    $scope.errors = false;

    $scope.Auth = function () {
        AuthService.login({username: $scope.username}).then(
            function () {
                $state.go("test");
            }, function (errors) {
                $scope.errors = errors;
            })
    }
});