app.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: "/pages/login/login.html",
            controller: 'LoginController',
            data: {
                requireLogin: false
            }
        })
        .state('test', {
            url: '/test',
            templateUrl: "/pages/test/test.html",
            controller: 'TestController',
            data: {
                requireLogin: true
            },
            resolve: {
                session: function (GameSession, $q, $state) {
                    var defer = $q.defer();

                    GameSession.getCurrentSession().then(function (gameSession) {
                        if (gameSession.status == 'CLOSED') {
                            $state.go('gameover');
                            defer.reject();
                        }
                        defer.resolve(gameSession);
                    }, function (errors) {
                        defer.reject(errors);
                    });

                    return defer.promise;
                }
            }
        })
        .state('gameover', {
            url: '/gameover',
            templateUrl: "/pages/gameover/gameover.html",
            controller: 'GameoverController',
            data: {
                requireLogin: true
            },
            resolve: {
                session: function (GameSession) {
                    return GameSession.getCurrentSession();
                }
            }
        });

    $urlRouterProvider.otherwise("/test");
});