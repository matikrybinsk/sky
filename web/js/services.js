app.factory('AuthService', function ($rootScope, $http, API, $q, $cookieStore) {

    function login(data) {
        var defer = $q.defer();

        $http.post(API.LOGIN_URL, data).then(
            function (result) {
                $rootScope.user = result['data']['data'];
                $cookieStore.put('user', $rootScope.user);

                applyUserData($rootScope.user);
                defer.resolve($rootScope.user);
            }, function (result) {
                defer.reject(result['data']['errors']);
            });

        return defer.promise;
    }

    function logout() {
        $cookieStore.remove('user');
        $rootScope.user = undefined;
        delete $http.defaults.headers.common['Authorization'];
    }

    function restoreUserData() {
        $rootScope.user = $cookieStore.get('user');
        applyUserData($rootScope.user);
    }

    function applyUserData(data) {
        if (data && data.token) {
            $http.defaults.headers.common['Authorization'] = 'Bearer ' + data.token;
        }
    }

    return {
        login: login,
        logout: logout,
        restoreUserData: restoreUserData
    };

});

app.factory('GameSession', function ($rootScope, $http, API, $q, $cookieStore) {
    var cookieKey = 'game_session_id';
    var currentSession;
    var currentQuestion;

    /**
     * Получает текущую сессию. Умеет также восстанавливать сессию при перезагрузке страницы.
     * @returns Promise
     */
    function getCurrentSession() {
        if (currentSession) {
            return $q.when(currentSession);
        }
        var gameSessionId = $cookieStore.get(cookieKey);
        var defer = $q.defer();

        $http.post(API.GAME_SESSION_GET, {game_session_id: gameSessionId}).then(
            function (result) {
                currentSession = result['data']['data'];
                $cookieStore.put(cookieKey, currentSession['session_id']);
                defer.resolve(currentSession);
            },
            function (result) {
                defer.reject(result['data']['errors']);
            }
        );

        return defer.promise;
    }

    /**
     * Загружает следующий вопрос
     * @returns Promise
     */
    function nextQuestion() {
        var defer = $q.defer();

        $http.post(
            API.GAME_SESSION_NEXT_ROUND,
            {
                session_id: currentSession.session_id
            }
        ).then(
            function (result) {
                currentQuestion = result['data']['data'];
                defer.resolve(currentQuestion);
            },
            function ($result) {
                defer.reject(result['data']['errors']);
            }
        );

        return defer.promise;
    }

    /**
     * Ответить на вопрос
     * @param answer_answer_id
     * @returns Promise
     */
    function answer(answer_answer_id) {
        var defer = $q.defer();

        $http.post(
            API.GAME_SESSION_ANSWER,
            {
                session_id: currentSession.session_id,
                word_id: currentQuestion.word.id,
                translation_type: currentQuestion.translation_type,
                answer_word_id: answer_answer_id
            }
        ).then(
            function (result) {
                defer.resolve(result['data']['data']);
            },
            function ($result) {
                defer.reject(result['data']['errors']);
            }
        );

        return defer.promise;
    }

    /**
     * Обнулить сессию (для начала новой игры)
     */
    function newSession() {
        $cookieStore.remove(cookieKey);
        currentSession = null;
        currentQuestion = null;
    }

    /**
     * Обовить статистику сессии
     */
    function updateSessionStats(data) {
        angular.forEach(data, function (value, key) {
            currentSession[key] = value;
        });
    }

    return {
        getCurrentSession: getCurrentSession,
        nextQuestion: nextQuestion,
        updateSessionStats: updateSessionStats,
        answer: answer,
        newSession: newSession
    };

});