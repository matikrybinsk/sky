app.constant("API", {
    LOGIN_URL: '/api/index.php?r=auth/login',
    GAME_SESSION_GET: '/api/index.php?r=game-session/get-session',
    GAME_SESSION_NEXT_ROUND: '/api/index.php?r=game-session/next-round',
    GAME_SESSION_ANSWER: '/api/index.php?r=game-session/answer'
});