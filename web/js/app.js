var app = angular.module('SkyEngApp', ['ui.router', 'ngCookies']);

app.run(function ($rootScope, $state, $cookieStore, $http, AuthService) {

    // Восстанавливаем данные из куков
    AuthService.restoreUserData();

    // Если не авторизованы, а страница требует, то идем на страницу логина
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, from, fromParams) {
        var requireLogin = toState.data.requireLogin;

        if (requireLogin && typeof $rootScope.user === 'undefined') {
            event.preventDefault();
            return $state.go('login');
        }
    });

    $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
        console.error(error);
    });

});